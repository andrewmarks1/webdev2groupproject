/**
 * @fileoverview - Manages the toggleable cheat code feature when user presses Shift+C, colour RGB values are displayed.
 * @author - Amy Nguyen, Andrew Marks 
 */

"use strict"
!(function() {

    /**
     * Removes text from all div with class divCol
     */
    function removeCheat(){
        const divColP = Array.from(document.querySelectorAll('div.divCol p'));
        divColP.forEach((divP) => divP.remove());
        }
    /**
     * Adds text paragprah to each div with their unique RGB values 
     */
    function addCheat(){
        let divCols = Array.from(document.querySelectorAll('div.divCol'));
        removeCheat()
        divCols.forEach((div) => {
            const rgbValue = document.createElement('p');
            rgbValue.textContent = div.style.backgroundColor;
            rgbValue.style.backgroundColor = 'rgba(230, 229, 299, 30%)';
            rgbValue.style.margin = 0;
            const rgbValueName = document.createElement('p');
            rgbValueName.classList.add('rgbName');
            rgbValueName.style.backgroundColor = 'rgba(230, 229, 299, 30%)';
            rgbValueName.style.margin = 0;
            rgbValueName.textContent = div.getAttribute('data-color');
            div.appendChild(rgbValue);
            div.appendChild(rgbValueName);
        });
    }


document.addEventListener('keydown', function(e){
    const isTextInput = e.target.tagName === 'INPUT' && e.target.type === 'text';
    if(!isTextInput && e.shiftKey && e.key === 'C'){
        document.body.classList.toggle('cheat');
    }
    document.body.addEventListener("keyup", function(e){
        if(e.shiftKey && e.key !== 'C'){
            return;
        }
        if(document.body.className ==='cheat'){
            addCheat();
        }
        else{
            removeCheat();
            }
        })
        }
    );
}());