/**
 * @fileoverview - Manages the setup of the game, ensuring that user enterse all valid inputs before commencing of game
 * @author - Amy Nguyen, Andrew Marks 
 */
"use strict"
!(function() {
document.addEventListener("DOMContentLoaded", function(){
    const startButton = document.querySelector('button');
    const inputArr = Array.from(document.querySelectorAll('input, select')) ;
    
    startButton.style.backgroundColor = inputArr[2].value;
    document.addEventListener("submit", function (e){ e.preventDefault();});

    inputArr[2].addEventListener("click", function(e){
        startButton.style.backgroundColor = inputArr[2].value;
    });
    /**
     * Checks if inputs are valid. If they are, enable 'Start Game!" button"
     */
    function validateInputs() {
        const inputArr = Array.from(document.querySelectorAll('input, select')) 
        startButton.disabled = !(inputArr[0].checkValidity() && inputArr[1].checkValidity());
    };
    inputArr.forEach((input) => input.addEventListener('input', validateInputs));
    });
}());
