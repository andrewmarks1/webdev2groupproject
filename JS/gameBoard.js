/**
 * @fileoverview - Manages the proper generation of the gameBoard once generated, with proper desired choices and inputs from user.
 * @author - Amy Nguyen, Andrew Marks
 */
"use strict"
!(function() {

    /**
     * This function returns the name of the highest value in an RGB value
     * @param {number} colour - takes in colour string of the divCol
     * @returns - returns the valueName of RGB which is the highest.
     */
    function checkHighestColour(colour){
        const colourArr = colour.split(",");
        const highestValue = Math.max(...colourArr);
        let highestValueName = "";
        if(highestValue === Number(colourArr[0])){highestValueName = "Red";}
        else if(highestValue === Number(colourArr[1])){highestValueName = "Green";}
        else{highestValueName = "Blue"};

        return highestValueName;
    }

    /**
     * Generates colours red green and blue given a range.
     * @param {number} range - the range 
     * @returns {string} - returns the RGB value after calculations.
     */
    const colourGenerator = (range) => {
        const randNum = Math.random(0, 255-range);
        const max = randNum + range;
        const min = randNum;
        
        const red = (Math.floor(Math.random() * (max-min) + min));
        const green = (Math.floor(Math.random() * (max-min) + min));
        const blue = (Math.floor(Math.random() * (max-min) + min));
            return `${red}, ${green}, ${blue}`;
        };
    
    /**
     * Generates a set of RGV values using colourGenerator depending on the difficulty level set.
     * @param {number} difficulty - Represents the difficulty set by the user in inputs.
     * @returns {string} - returns the RGB values with specific range taken into consideration.
     */
    function generateDivColours(difficulty){
        let colour;
        if(difficulty === 0){
            colour = colourGenerator(255);
        }

        else if(difficulty === 1){
            colour =colourGenerator(80);
        }

        else if(difficulty === 2){
            colour = colourGenerator(40);
        }
        else if(difficulty === 3){
            colour =colourGenerator(10);
        }
        return colour;
    }
    /**
     * Creates the gameBoard with specified user size.
     * @param {number} size - This makes the size of the table, sizeXsize that user has chosen.
     */
        function generateTable(board, size, level){
            const divsToRemove = board.querySelectorAll('.divRow');
            divsToRemove.forEach(divRow => divRow.remove());
            for(let i = 0; i < size; ++i){
                const divRow = document.createElement('div');
                divRow.classList.add('divRow');
    
                for(let j = 0; j < size; ++j){
                    const divCol = document.createElement('div');
    
                    divCol.addEventListener('click', function(){
                        divCol.classList.toggle('clicked');
                    });
    
                    divCol.classList.add('divCol');
    
                    let color = generateDivColours(Number(level));
                    divCol.style.backgroundColor = (`rgb(${color}`);
                    divCol.setAttribute('data-color', checkHighestColour(color));
                    divRow.appendChild(divCol);
                }
                board.insertBefore(divRow, document.getElementById('searchTiles'));
            }
        }



document.addEventListener("DOMContentLoaded", function(){

    const startButton = document.querySelector('button');
    const submitGuess = document.getElementById('submitGuess');
    const boardSection = document.getElementById('gameBoard_section');
    const boardSize = document.getElementById('boardSize');
    const difficulty = document.getElementById('difficulty');

    const setUpInputsArr = [...document.getElementsByClassName('userChoice')];

    startButton.addEventListener('click', function(){
        startButton.disabled = true;
        submitGuess.disabled = false;
        boardSection.disabled = false;

        setUpInputsArr.forEach((input) => input.disabled = true);
        document.body.classList.remove('cheat');
        const divRows = Array.from(document.getElementsByClassName('divRow'));
        generateTable(boardSection, boardSize.value, difficulty.value);
        document.getElementById('searchTiles').style.visibility = 'hidden';
        divRows.forEach((div) => div.style.opacity = '100%');
    });
    submitGuess.addEventListener('click', function(e){
        const divRows = Array.from(document.getElementsByClassName('divRow'));

        setUpInputsArr.forEach((input) => input.disabled = false);
        startButton.disabled = false;
        submitGuess.disabled = true;
        divRows.forEach((div) => div.style.opacity = '10%');
    });
    })
}());
