/**
 * @fileoverview - Manages the highscore concept of the game, as well as inside local storage.
 * @author - Andrew Marks, Amy Nguyen
 */
"use strict";

!(function() {
    
    /**
     * This function returns the current score of the player if they selected the right div with the highest colour of the game.
     * @param {String} curColour - represents the current selected colour of the game.
     * @returns - returns the number of divs that had been rightfully selected.
     */
    const numCorrect = (curColour) => {
        const numSelected = Array.from(document.querySelectorAll('div.clicked'));
        const correctGuesses = numSelected.filter((div) => div.getAttribute('data-color') === curColour);
        let numCorrects = correctGuesses.length;
        return numCorrects;
    } 

    /**
     * This method calculates the number of current divs that are correct, depending on chosen colour of the game.
     * @param {String} curColour - represents the current seleccted colour of the game.
     * @returns - returns the number of current correct divs of the round.
     */
    const currentRights = (curColour)=>{
        const allDivCols = Array.from(document.querySelectorAll('div.divCol'));
        const rights = allDivCols.filter((divs) => divs.getAttribute('data-color') === curColour);
        return rights.length;
    }

    /**
     * This method calculates the score depending on the number of correct selected boxes, how many divs the user has 
     * selected in total, the board size, as well as the difficulty of the round chosen.
     * @param {number} numCorrects - represents number of correct selected by user.
     * @param {number} numSelected - represents number of all seelcted by user.
     * @param {number} boardSize - represents the board size of the current game.
     * @param {number} difficulty - represents the current difficulty of the game.
     * @returns - returns the score the user has received for the round.
     */
    function getScore(numCorrects, numSelected, boardSize, difficulty) {
        const percent = ( 2 * numCorrects - numSelected ) /(boardSize * boardSize);
        return Math.floor(percent * 100 * boardSize * (difficulty + 1));
    }

    /**
     * Displays a completion message once user submits their guess.
     * @param {string} curColour - represents the current colour of the game
     */
    function displayCompletionMessage(curColour, score){
        const numSelected = Array.from(document.querySelectorAll('div.clicked'));
        const results = document.getElementById('searchTiles');
        results.style.visibility = 'visible';
        results.textContent = (`Searching for ${curColour} tiles! You've selected ${numSelected.length} tiles. There were ${currentRights(curColour)} correct tiles.
                                You've selected ${numCorrect(curColour)} correct boxes, and ${numSelected.length-numCorrect(curColour)} wrong boxes. You've scored ${score} points! :)`);
    }

    /**
     * Generates player li with current player name
     * @param {string} pName - The current player name of the round.
     * @returns {playerNameLI} - the new LI with the current player name.
     */
    function generateHighscorePlayer(pName){
        const playerNameLI = document.createElement('li');
        playerNameLI.textContent = pName;
        playerNameLI.classList.add('Score');
        return playerNameLI;
    }

    /**
     * This function erases the HighScore leaderboard and sorts it with the score of the player that just played the game.
     * @param {Array} highScores.
     */
    function updateHighScores(highScores){
        const playerUL = document.getElementById('players');
        const scoresUL = document.getElementById('scores');
        playerUL.textContent = undefined;
        scoresUL.textContent = undefined;

        highScores.forEach(player =>{
            const template = document.getElementById('Score-template').content;
            const newResults = template.cloneNode(true);
            const playerResultName = newResults.querySelector('.playerName');
            const playerResultScore = newResults.querySelector('.Score');

            playerResultName.textContent = player.name;
            playerResultScore.textContent = player.score;

            playerUL.appendChild(playerResultName);
            scoresUL.appendChild(playerResultScore);
        });
    }

    /**
     * Generates score LI with the score that the player recieved for round.
     * @param {string} - represents the score of the round.
     * @return {playerScoreLI} - The new LI with the score
     */
    function generateHighscoreScore(Score){
        const playerScoreLI = document.createElement('li');
        playerScoreLI.textContent = Score;
        playerScoreLI.classList.add('Score');
        return playerScoreLI;
    }

    /**
     * Flashes highscore header when currPlayer is the current winner of the highscore board, only when multiple players are playing.
     * @param {number} score - the current player's score
     */
    function blinkHighscores(score){
        let LIsArr = Array.from(document.getElementsByClassName('Score'));
        const header = document.getElementById('highscoreHeader');
        if(LIsArr.length === 1){
            return;
        }
            if(score > Number(LIsArr[1].textContent)){
                let interval;
                let stopNum = 0;
                interval = setInterval(() => {header.classList.toggle('green'); stopNum++; 
                if(stopNum === 10){
                    clearInterval(interval);
                }}, 150);
            }
    }
    

document.addEventListener('DOMContentLoaded', function(){       
    let curColour;
    let boardSize;
    let difficulty;
    //gives values when start button is clicked. 
    document.querySelector('button').addEventListener('click', function(){
        curColour = document.querySelector('select').value;
        boardSize =  document.getElementById('boardSize').value;
        difficulty = document.getElementById('difficulty').value;
    });
    // const numSelected = Array.from(document.querySelectorAll('div.clicked'));
    let highScores = JSON.parse(localStorage.getItem('highscores')) || [];
    const playerUL = document.getElementById('players');
    const scoresUL = document.getElementById('scores');

    highScores.forEach((el) => {
        playerUL.appendChild(generateHighscorePlayer(el['name']));
        scoresUL.appendChild(generateHighscoreScore(el['score']));
        });
    document.getElementById('submitGuess').addEventListener("click", function(){
        function generateHighscoreObjects(){
            const player = {name: newName, score: score};
            return player;
            }
        const numSelected = Array.from(document.querySelectorAll('div.clicked'));
        const score = Number(getScore(numCorrect(curColour),numSelected.length, boardSize, difficulty));

        displayCompletionMessage(curColour, score);

        let newName = document.getElementById('pName').value;
        let userExists = false;
        highScores.forEach( player => {
            if(player.name === newName){
                player.score += score;
                userExists = true;
            }
        });

        if(!userExists){
            let currPlayer = generateHighscoreObjects();
            highScores.push(currPlayer);
        }

        highScores.sort( (a, b) => b.score - a.score);

        if(highScores.length === 11){
            highScores.pop();
        }
        updateHighScores(highScores);
        localStorage.setItem('highscores',JSON.stringify(highScores));
        //gets the  player object that matches the current player of the game.
        let p = highScores.filter((element) => element.name === document.querySelector('input').value);
        blinkHighscores(p[0].score);
    });

    document.getElementById('clearLocal').addEventListener('click', function(){
        document.getElementById('players').textContent = undefined;
        document.getElementById('scores').textContent = undefined;
        localStorage.clear();
        highScores = [];
        const liArr = Array.from(document.getElementsByTagName('li'));
        liArr.forEach((li) => li.remove());
    });

    const scoreList = document.getElementsByTagName('h3')[1];
    scoreList.addEventListener('click', function(){
        scoreList.classList.toggle('a');
            if(scoreList.classList.contains('a')){
                highScores.sort( (a, b) => a.score - b.score);
            } else{
                highScores.sort( (a, b) => b.score - a.score);
            }
            updateHighScores(highScores);
        });
        
    });
    
}());
