# WebDev2GroupProject

This project is a score based colour guessing game, in which the difficulty is manipulable by the user. Player's progress is logged onto the highscore board, which is stored on the user's local storage.



Instructions:

Startup:

Once the website is loaded, the game will begin to ask you for your name, your desired board size, colour you wish the game round to be focused on, as well as the difficulty you wish to play in.
The game can only begin once all inputs are valid.

GamePlay:

Once the game board is loaded, user must select the boxes in which they think is the must prominent according to the colour they had picked during setup. 
If lost, user can simultaneously press and let go Shift+C, which will enable cheatmode! This will show the RGB values, along with the name of the colour most prominent of each boxes.
Once user is done with selecting all boxes, and believe that they had done so correctly, they can press the button "Submit your guess". This will show a completion method, along with their score.

Once submitted, users can change their inputs, and play again. This is only permitted if the user has submitted their guess, and vice versa.

Results:

The message shown once guess submitted shows the colour focused, how many titles the user selected along wit how many correct ones. It also shows how many correct boxes the user has seleted, as well as how many wrongs. It then shows the points.

Highscore board:

This board is the list of all players of the game, as well as their respective scores. A user will always be saved in highscore board, and their score will keep appending to their name as long as
their name exists on the highscore board. The clear board button clears the highscore history, and restarts the competition.

A feature about the highscore board is that if the player is the top winner of the game, the highscore banner will flash momentarily, indicating that they are placed #1 of the highscore board.




